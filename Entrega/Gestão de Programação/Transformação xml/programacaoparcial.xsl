<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:c="http://www.dei.isep.ipp.pt/lprog">	
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="c:programacao">
		<title>NETFLAX</title>	
		<programacao>
			<xsl:apply-templates select="c:programa"/>
		</programacao>
	</xsl:template>
	<xsl:template match="c:programa">		
			<xsl:apply-templates select="c:tipo"/>
	</xsl:template>
	<xsl:template match="c:tipo">
		<xsl:if test="c:serie">
			<programa>
			<img src="{../c:image}"  width="180" height="260"/>
			<nome><xsl:value-of select="../c:nome"/></nome>			
			<tipo><xsl:apply-templates select="c:serie"/></tipo>			
			<atores><xsl:apply-templates select="../c:atores"/></atores>
			<categorias><xsl:apply-templates select="../c:categorias"/></categorias>
			<realizador><xsl:value-of select="../c:realizador"/></realizador>
			<produtora><xsl:value-of select="../c:produtora"/></produtora>
			<duracao><xsl:value-of select="../c:duracao"/></duracao>
			</programa>
		</xsl:if>
	</xsl:template>
	<xsl:template match="c:categorias">
		<xsl:for-each select="c:categoria">
			<categoria><xsl:value-of select="."/></categoria>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="c:atores">
		<xsl:for-each select="c:ator">
			<ator><xsl:value-of select="."/></ator>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="c:serie">
	<serie>
		<temporada><xsl:value-of select="c:temporada"/></temporada>
		<episodio><xsl:value-of select="c:episodio"/></episodio> 	
	</serie>
	</xsl:template>
	</xsl:stylesheet>													