<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:c="http://www.dei.isep.ipp.pt/lprog">	
	<xsl:output method="html"/>
	<xsl:template match="/c:programacao">
		<html>
			<head>
				<title>NETFLAX</title>
			</head>
			<body>
				<article class="accordion">
					<section id="acc1">
						<h2><a href="#acc1">LISTAGEM PROGRAMAS</a></h2>			
						<xsl:apply-templates select="c:programa">									
							<xsl:sort select="c:nome" data-type="text" order="ascending"/>
						</xsl:apply-templates>
						<p class="accnav"><a href="#acc2">Pr�ximo</a></p>
					</section>
					<xsl:call-template name="series"/>
					<xsl:call-template name="filmes"/>
					<xsl:call-template name="documentos"/>
					<xsl:call-template name="listatores"/>
					<section id="fim">
						<h2><a href="#acc1">Voltar ao Inicio</a></h2>
					</section>
				</article>				
				<style>
					<xsl:call-template name="style"/>
				</style>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="c:programa">
		<table align="center">
			<tr>
				<td colspan="4">
					<h2>
						<xsl:value-of select="c:nome"/>
					</h2>
				</td>
			</tr>
			<tr>
				<xsl:apply-templates select="c:tipo"/>
			</tr>
		</table>
		<table>
			<tr>
				<td colspan="4">
					<img src="{c:image}"  width="180" height="260"/>
				</td>
				<td colspan="3">
					<p><b>Categoria:</b></p>
					<xsl:apply-templates select="c:categorias"/>					
					<p><b>Realizador:</b><xsl:value-of select="c:realizador"/></p>
					<p><b>Produtora:</b><xsl:value-of select="c:produtora"/></p>
					<p><b>Dura��o:</b><xsl:value-of select="c:duracao"/></p>
				</td>
				<td style="min-width:50px" colspan="2"/>
				<td>
					<p><b>Ator:</b></p>
				<xsl:apply-templates select="c:atores"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="c:categorias">
		<xsl:for-each select="c:categoria">
			<p><xsl:value-of select="."/></p>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="c:atores">
		<xsl:for-each select="c:ator">
			<p><xsl:value-of select="."/></p>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="c:tipo">
		<xsl:if test="c:serie"><b>Serie -</b>
		<xsl:apply-templates select="c:serie"/></xsl:if>
		<xsl:if test="c:filme"><b>Filme</b></xsl:if>
		<xsl:if test="c:documentario"><b>Document�rio</b></xsl:if>
	</xsl:template>
	<xsl:template match="c:serie">
		Temporada: <xsl:value-of select="c:temporada"/>
		/ Epis�dio: <xsl:value-of select="c:episodio"/>
	</xsl:template>
	<xsl:template name="listatores">
		<xsl:for-each select="//c:ator[not(preceding::c:ator/. = .)]">
			
			<xsl:variable name="ator" select="." />
			<xsl:variable name="link" select="concat('#acc',position()+1)" />
			<xsl:variable name="i" select="concat('acc',position()+1)" />
			<xsl:variable name="next" select="concat('#acc',position()+2)" />
			
			<section id="{$i}">
				<h2><a href="{$link}"><xsl:value-of select="."/></a></h2>
				<h3>Participa em:</h3>
				<xsl:for-each select="//c:ator">
					<xsl:if test=".=$ator">
						<p align="center">
							<xsl:apply-templates select="../../c:nome"/><br/>
							<xsl:apply-templates select="../../c:tipo"/>
						</p>
					</xsl:if>
				</xsl:for-each>
				<p class="accnav"><a href="{$next}">Pr�ximo</a></p>
			</section>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="filmes">			
		<section id="acc300">
			<h2><a href="#acc300">Filmes</a></h2>
			<xsl:for-each select="//c:tipo">
				<xsl:if test="c:filme">
					<p align="center">
						<p><xsl:call-template name="corpo"/><br/>
						</p>
					</p>
				</xsl:if>
			</xsl:for-each>
		</section>
	</xsl:template>
	<xsl:template name="series">
	<section id="acc400">
		<h2><a href="#acc400">S�ries</a></h2>
			<xsl:for-each select="//c:tipo">
				<xsl:if test="c:serie">
					<p align="center">
						<p><xsl:call-template name="corpo"/><br/>
						</p>
					</p>
				</xsl:if>
			</xsl:for-each>
		</section>
	</xsl:template>
	<xsl:template name="documentos">			
		<section id="acc200">
			<h2><a href="#acc200">Document�rio</a></h2>
			<xsl:for-each select="//c:tipo">
				<xsl:if test="c:documentario">
					<p align="center">
						<p><xsl:call-template name="corpo"/><br/>
						</p>
					</p>
				</xsl:if>
			</xsl:for-each>
		</section>
	</xsl:template>
	<xsl:template name="corpo">
		<table align="center">
			<tr>
				<td colspan="4">
					<h2>
						<xsl:value-of select="../c:nome"/>
					</h2>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td colspan="4">
					<img src="{../c:image}"  width="180" height="260"/>
				</td>
				<td colspan="3">
					<p><b>Categoria:</b></p>
					<xsl:apply-templates select="../c:categorias"/>					
					<p><b>Realizador:</b><xsl:value-of select="../c:realizador"/></p>
					<p><b>Produtora:</b><xsl:value-of select="../c:produtora"/></p>
					<p><b>Dura��o:</b><xsl:value-of select="../c:duracao"/></p>
				</td>
				<td style="min-width:50px" colspan="2"/>
				<td>
					<p><b>Ator:</b></p>
					<xsl:apply-templates select="../c:atores"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name="style">
		body
		{
		font-family: "Segoe UI", arial, helvetica, freesans, sans-serif;
		font-size: 90%;
		color: #333;
		background-color: #ccccb3;
		margin: 10px;
		z-index: 0;
		}
		
		h1
		{
		font-size: 1.5em;
		font-weight: normal;
		margin: 0;
		}
		
		h3
		{
		text-align: center;
		}
		
		h2
		{
		text-align: center;
		font-size: 1.3em;
		font-weight: normal;
		margin: 2em 0 0 0;
		}
		
		p
		{
		margin: 0.6em 0;
		}
		
		p.accnav
		{
		font-size: 1.1em;
		text-transform: uppercase;
		text-align: right;
		}
		
		p.accnav a
		{
		text-decoration: none;
		color: #999;
		}
		
		article.accordion
		{
		display: block;
		width: 50em;
		padding: 0.5em 0.5em 1px 0.5em;
		margin: 0 auto;
		background-color: #666;
		border-radius: 5px;
		box-shadow: 0 3px 3px rgba(0,0,0,0.3);
		}
		
		article.accordion section
		{
		display: block;
		width: 48em;
		height: 2em;
		padding: 0 1em;
		margin: 0 0 0.5em 0;
		color: #333;
		background-color: #333;
		overflow: hidden;
		border-radius: 3px;
		}
		
		article.accordion section h2
		{
		font-size: 1em;
		font-weight: bold;
		width: 100%;
		line-height: 2em;
		padding: 0;
		margin: 0;
		color: #ddd;
		}
		
		article.accordion section h2 a
		{
		display: block;
		width: 100%;
		line-height: 2em;
		text-decoration: none;
		color: inherit;
		outline: 0 none;
		}
		
		article.accordion section,
		article.accordion section h2
		{
		-webkit-transition: all 1s ease;
		-moz-transition: all 1s ease;
		-ms-transition: all 1s ease;
		-o-transition: all 1s ease;
		transition: all 1s ease;
		}
		
		article.accordion section:target
		{
		height: auto;
		background-color: #fff;
		}
		
		article.accordion section:target h2
		{
		font-size: 1.6em;
		color: #333;
		}
	</xsl:template>
	
</xsl:stylesheet>											