<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns="http://www.dei.isep.ipp.pt/lprog" 
           xmlns:xs="http://www.w3.org/2001/XMLSchema"
           targetNamespace="http://www.dei.isep.ipp.pt/lprog" 
           elementFormDefault="qualified">
    
  <xs:element name="relat�rio" type="TRelatorio"/>

  <xs:complexType name="TRelatorio">
    <xs:sequence>
      <xs:element name="p�ginaRosto" type="TpaginaRosto"/>
      <xs:element name="corpo" type="TCorpo"/>
      <xs:element name="anexos" type="TSeccao" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
    <xs:attribute name="id" type="xs:ID" use="required"/>
  </xs:complexType>

    <xs:complexType name="TpaginaRosto">
      <xs:sequence>
        <xs:element name="tema" type="xs:string"/>
        <xs:element name="disciplina" type="TDisciplina"/>
        <xs:element name="autor" type="TAluno" maxOccurs="3"/>
        <xs:element name="data" type="xs:date"/>
        <xs:element name="turmaPL" type="TTurma"/>
        <xs:element name="profPL" type="TProfPL"/>
        <xs:element name="logotipoDEI" type="xs:anyURI"/>
      </xs:sequence>
    </xs:complexType>
 
  <xs:complexType name="TCorpo">
    <xs:sequence>
      <xs:element name="introdu��o" type="TSeccao"/>
      <xs:element name="sec��es" type="TOutrasSeccoes"/>
      <xs:element name="conclus�o" type="TSeccao"/>
      <xs:element name="refer�ncias" type="TReferencias"/>
    </xs:sequence>
    <xs:attribute name="id" type="xs:string" use="required"/>
  </xs:complexType>
  
  <xs:complexType name="TSeccao">
        <xs:sequence>
            <xs:element name="bloco" type="TBloco" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="tituloSec��o" type="xs:string" use="required"/>
        <xs:attribute name="id" type="TidSec" use="required"/>
    </xs:complexType>
    
    <xs:complexType name="TOutrasSeccoes">
        <xs:all>
            <xs:element name="an�lise" type="TSeccao"/>
            <xs:element name="linguagem" type="TSeccao"/>
            <xs:element name="transforma��es" type="TSeccao"/>
        </xs:all>
     </xs:complexType>
    
    <xs:complexType name="TBloco">
        <xs:choice maxOccurs="unbounded">
            <xs:element name="par�grafo" type="Tparagrafo" />
            <xs:element name="listaItems" type="TListaItems"/>
            <xs:element name="figura" type="TFigura"/>
            <xs:element name="codigo" type="TCodigo"/>
        </xs:choice>
    </xs:complexType>
    
    <xs:complexType name="Tparagrafo" mixed="true">
        <xs:choice minOccurs="0" maxOccurs="unbounded">
            <xs:element name="cita��o" type="RefRB"/>
            <xs:element name="bold" type="xs:string"/>
            <xs:element name="it�lico" type="xs:string"/>
            <xs:element name="sublinhado" type="xs:string"/>
        </xs:choice>
    </xs:complexType>
    
    <xs:complexType name="TListaItems">
        <xs:sequence>
            <xs:element name="item" type="Titem" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>
    
    <xs:complexType name="Titem">
        <xs:simpleContent>
            <xs:extension base="xs:string">
                <xs:attribute name="label" type="xs:normalizedString" use="optional"/>
            </xs:extension>
        </xs:simpleContent>
        
    </xs:complexType>
    
    <xs:complexType name="TFigura">
        <xs:attribute name="src" type="xs:anyURI"/>
        <xs:attribute name="descri��o" type="xs:string"/>
    </xs:complexType>
    
    <xs:complexType name="TCodigo">
        <xs:sequence>
            <xs:element name="linha" type="xs:string" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="id" type="xs:ID"/>
    </xs:complexType>
    
    <xs:complexType name="TAluno" mixed="true">
        <xs:sequence>
            <xs:element name="nome" type="xs:string"/>
            <xs:element name="n�mero" type="TNum"/>
            <xs:element name="mail" type="TEMail"/>
        </xs:sequence>
        <xs:attribute name="id" type="xs:ID"/>
    </xs:complexType>
    
    <xs:complexType name="TDisciplina">
        <xs:sequence>
            <xs:element name="designa��o" type="xs:string"/>
            <xs:element name="anoCurricular" type="xs:int" fixed="2"/>
            <xs:element name="sigla" type="xs:string"/>
        </xs:sequence>
        <xs:attribute name="id" type="xs:string"/>
    </xs:complexType>
    
    <xs:complexType name="TReferencias">
        <xs:choice maxOccurs="unbounded">
            <xs:element name="refBibliogr�fica" type="TRBib"/>
            <xs:element name="refWeb" type="TRWeb"/>
        </xs:choice>
    </xs:complexType>
    
    <xs:complexType name="TRBib">
        <xs:sequence maxOccurs="unbounded">
             <xs:element name="t�tulo" type="xs:string"/>
            <xs:element name="dataPublica��o" type="xs:string"/>
            <xs:element name="autor" type="xs:string" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="idRef" type="TIdRefB" use="required"/>
    </xs:complexType>
    
    <xs:complexType name="TRWeb">
        <xs:sequence maxOccurs="unbounded">
            <xs:element name="URL" type="xs:anyURI"/>
            <xs:element name="descri��o" type="xs:string"/>
            <xs:element name="consultadoEm" type="xs:date"/>
        </xs:sequence>
        <xs:attribute name="idRef" type="TIdRefW" use="required"/>
    </xs:complexType>
	
	
    <xs:simpleType name="TIdRefB">
        <xs:restriction base="xs:ID">
            <xs:pattern value="RefB_\w+"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="TIdRefW">
        <xs:restriction base="xs:ID">
            <xs:pattern value="RefW_\w+"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="RefRB">
        <xs:restriction base="xs:IDREF">
            <xs:pattern value="RefB_\w+"/>
            <xs:pattern value="RefW_\w+"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="TidSec">
        <xs:restriction base="xs:ID">
            <xs:pattern value="SEC[0-9]{3}"/>
            <xs:pattern value="ANX[0-9]{3}"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="TTurma">
        <xs:restriction base="xs:token">
            <xs:pattern value="2D[A-N]"/>
            <xs:pattern value="2N[A-C]"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
