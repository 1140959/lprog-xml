<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:c="http://www.dei.isep.ipp.pt/lprog">
	
	<xsl:output method="html"/>
	<xsl:template match="/c:relatório">
		<html>
			<head>
				<title>Relatório LPROG</title>
			</head>
			<body>
				<page size="A4"><xsl:apply-templates select="c:páginaRosto"/></page>
				<br/>
				<page size="A4">
					<div align="left">
						<xsl:call-template name="indice"/>
					</div></page>
					<br/>
					<page size="A4">
						<xsl:apply-templates select="c:corpo"/>
					</page>
					<br/>
					<style>
						<xsl:call-template name="style"/>
					</style>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="indice">
		<div style="position: relative;top: 20px;left: 40px;">
			<b><h2><i><u>Índice</u></i></h2></b>				
			<xsl:for-each select="//@tituloSecção">
				<p><xsl:value-of select="."/></p>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template match="c:páginaRosto">
		<img src="{c:logotipoDEI}" width="300" height="100"/>
		<div align="center">
			<h1>Tema: </h1>
		</div>
		<div align="center">
			<h1><p><xsl:value-of select="c:tema"/></p></h1>
		</div>
		
		<div id="container">
			<div sytle="left" style="absolute">
				<xsl:apply-templates select="c:disciplina"/>
			</div>
			<br/>
			<div id="right">
				<xsl:apply-templates select="c:autor"/>
				<br/>
				<p><b>Data: </b><xsl:value-of select="c:data"/></p>
				<p><b>Docente: </b><xsl:value-of select="c:profPL"/></p>
			</div>
		</div>
		<br/>
	</xsl:template>
	<xsl:template match="c:disciplina">	
		<p><b>Ano Curricular: </b><xsl:value-of select="c:anoCurricular"/></p>
		<p><b>Disciplina: </b><xsl:value-of select="c:designação"/></p>
		<p><b>Sigla Disciplina: </b><xsl:value-of select="c:sigla"/></p>
	</xsl:template>
	<xsl:template match="c:autor">
		<p><b>Nome: </b><xsl:value-of select="c:nome"/>
			<b> Número: </b><xsl:value-of select="c:número"/>
		<b> Email: </b><xsl:value-of select="c:mail"/></p>
	</xsl:template>
	<xsl:template match="c:corpo">
		<page size="A4">
			<p><xsl:apply-templates select="c:introdução"/></p>
		</page>
        <br/>
		<p><xsl:apply-templates select="c:secções"/></p>
		<br/>
		<page size="A4">
			<xsl:apply-templates select="c:conclusão"/>
		</page>
        <br/>
		<page size="A4">
			<xsl:apply-templates select="c:referências"/>
		</page>
		<page size="A4">
			<xsl:apply-templates select="//c:anexos"/>					
			<br/>
			<br/>
		</page>
	</xsl:template>
	<xsl:template match="c:introdução">
		<xsl:apply-templates select="c:bloco"/>
	</xsl:template>
	<xsl:template match="c:secções">
		<page size="A4">
		<xsl:apply-templates select="c:análise"/></page>
        <br/>
		<page size="A4">
		<xsl:apply-templates select="c:linguagem"/></page>
		<br/>
		<page size="A4">
		<xsl:apply-templates select="c:transformações"/></page>
	</xsl:template>
	<xsl:template match="c:análise">
		<xsl:apply-templates select="c:bloco"/>
	</xsl:template>
	<xsl:template match="linguagem">
		<p><xsl:value-of select="c:bloco"/></p>
	</xsl:template>
	<xsl:template match="transformações">
		<p><xsl:value-of select="c:bloco/paragrafo"/></p>
	</xsl:template>
	<xsl:template match="c:conclusão">
		<p><xsl:apply-templates select="c:bloco"/></p>
	</xsl:template>
	<xsl:template match="c:referências">
		<div style="position: relative;top: 20px;left: 40px;">
			<p><b><h2><i><u>
				Referências
			</u></i></h2></b></p>
		</div>
		<div style="position: relative;top: 40px;left: 20px;width:750px;">
			<xsl:if test="c:refWeb">
			<xsl:apply-templates select="c:refWeb"/></xsl:if>
			<xsl:if test="c:refBibliográfica">
			<xsl:apply-templates select="c:refBibliográfica"/></xsl:if>
		</div>
	</xsl:template>
	<xsl:template match="c:anexos">
		<div style="position: relative;top: 20px;left: 40px;">
			<p><h2><i><u>
				<xsl:value-of select="@tituloSecção"/>
			</u></i></h2></p>
		</div>
		<div style="position: relative;top: 40px;left: 20px;width:750px;">
			<xsl:for-each select="c:bloco/c:figura">
				<p><a href="{@src}"><xsl:value-of select="@descrição"/></a></p>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template match="c:refBibliográfica">
		<p><b>Título: </b><xsl:value-of select="c:título"/></p>
		<p><b>DataPublicação: </b><xsl:value-of select="c:dataPublicação"/></p>
		<xsl:for-each select="c:autor">
			<p><b>Autor: </b>
			<xsl:value-of select="."/></p>
		</xsl:for-each>
		<br/>
	</xsl:template>
	<xsl:template match="c:refWeb">
		<p><b>URL: </b><xsl:value-of select="c:URL"/></p>
		<p><b>Discrição: </b><xsl:value-of select="c:descrição"/></p>
		<p><b>ConsultadoEm: </b><xsl:value-of select="c:consultadoEm"/></p>
		<br/>
	</xsl:template>
	<xsl:template match="c:bloco">
		<div style="position: relative;top: 20px;left: 40px;">
			<p><h2><b><i><u>
				<xsl:value-of select="../@tituloSecção"/>
			</u></i></b></h2></p>
		</div>
		<div style="position: relative;top: 40px;left: 20px;width:750px;">
			<xsl:if test="c:parágrafo">
				<xsl:for-each select="c:parágrafo">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="c:codigo">
				<xsl:for-each select="c:linha">
					<p><xsl:value-of select="c:linha"/></p>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="c:figura">
				<xsl:value-of select="@descrição"/>
				<img src="{c:figura/@src}" width="400" height="600"/>
			</xsl:if>
			<xsl:if test="c:listaItems">
				<xsl:apply-templates select="c:listaItems"/>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template match="c:listaItems">
		<xsl:for-each select="c:item">
			<p><b><xsl:value-of select="@label"/>: </b>
			<xsl:value-of select="."/></p>
		</xsl:for-each>
		</xsl:template>
		
		<xsl:template name="style">
		#container{width:100%;}
		#left{float:left;width:300px;}
		#right{float:right;width:500px;}
		#center{margin:0 auto;width:500px;}
		body {
		background: rgb(204,204,204); 
		}
		page {
		background: white;
		display: block;
		margin: 0 auto;
		margin-bottom: 1.5cm;
		box-shadow: 2 2 2.5cm rgba(1.5,	1.5,1.5,1.5);
		}
		page[size="A4"] {  
		width: 21cm;
		height: 29.7cm; 
		}
		page[size="A4"][layout="portrait"] {
		width: 29.7cm;
		height: 21cm;  
		}
		@media print {
		body, page {
		margin: 0;
		box-shadow: 0;
		}
		section.sec{}
		}
		</xsl:template>
		</xsl:stylesheet>										