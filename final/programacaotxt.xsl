<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:t="http://www.dei.isep.ipp.pt/lprog">
<xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/t:programacao">			
<xsl:apply-templates select="t:programa"/>
</xsl:template>
<xsl:template match="t:programa">
<xsl:apply-templates select="t:tipo"/>
Nome:<xsl:value-of select="t:nome"/>		
Categoria:
<xsl:for-each select="t:categorias">
<xsl:value-of select="concat(' ',.)"/>
</xsl:for-each>
Realizador:<xsl:value-of select="t:realizador"/>
Ator:
<xsl:for-each select="t:atores">
<xsl:value-of select="concat(' ',.)"/>
</xsl:for-each>
Produtora:<xsl:value-of select="t:produtora"/>
Rating:<xsl:value-of select="@rating"/>
Duracao:<xsl:value-of select="t:duracao"/>
<xsl:text>&#xa;&#xa;&#xa;</xsl:text>
</xsl:template>
<xsl:template match="t:tipo">
	
<xsl:if test="t:serie">Serie
<xsl:apply-templates select="t:serie"/></xsl:if>
<xsl:if test="t:filme">Filme</xsl:if>
<xsl:if test="t:documentario">Documentario</xsl:if>
</xsl:template>
<xsl:template match="t:serie">
Temporada:<xsl:value-of select="t:temporada"/>
Episodio:<xsl:value-of select="t:episodio"/>
</xsl:template>
</xsl:stylesheet>